<?php


function getRequest( String $url, array $params = []){
    
    $http = curl_init($url);

    curl_setopt($http, CURLOPT_RETURNTRANSFER, true);
    if( ! empty( $params ) ){
        curl_setopt($http, CURLOPT_POSTFIELDS, $post);
    }
    
    $rawResponse = curl_exec($http);

    handleResponse($http, $rawResponse);

    curl_close($http);

    return json_decode($rawResponse);
}

function getFleetLastLocation($key, $url = URL_FLEET_REQUEST){

    $url = str_replace("{key}",$key,$url);

    $response = getRequest( $url );

   return (new Fleet())->loadFromResponse($response);
}

function getDailyTrack(array $track, $url = URL_TRACK_REQUEST){

    foreach($track as $requestKey => $requestValue ){
        $url = str_replace($requestKey, $requestValue, $url);
    }

    $response = getRequest($url);

   return (new Track())->loadFromResponse($response);

}


function handleResponse($http, $response){

    $http_status = (int) curl_getinfo($http, CURLINFO_HTTP_CODE);

    $response = json_decode($response);

    if($response === null) {
        throw new Exception( 'Code ' . $http_status . ': Could not process correctly the response');
    }

    if( empty($response->response) ) {
        throw new Exception( 'Code ' . $http_status . ': response is empty ');
    }

    if( $http_status != 200 && isset($response->errormessage) ){
        throw new Exception( 'Code ' . $http_status . ': ' . $response->errormessage );
    }

    if( $http_status != 200 ){
        throw new Exception( 'Code ' . $http_status . ': Could not process the error code, please get some support');
    }
}

function errorRequest( String $message, $json = true){
    echo json_encode([
        'status' => 'fail',
        'message' => $message
    ]);
    die;
}