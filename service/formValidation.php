<?php

$errors = [];

function validateFleetRequest(){
    if(! isPostRequest()){
      return ['Wrong request type'];  
    }
    if(! requiredPostValues(['fleet'])){
      return ['Required value missing from request'];  
    }
    return [];
}

function validateTrackRequest(){

    if (! isPostRequest()) {
        return ['Wrong request type'];
    }

    if (! requiredPostValues(['id','start','key'])) {
        return ['Required value missing from request'];
    }

    if (! isValidDate( $_POST['start'] )) {
        return ['Invalid date'];
    }

    return [];
}

function isValidDate( string $stringDate){

    $date = DateTime::createFromFormat(DATE_FORMAT, $stringDate);

    if (empty($date)) {
        return false;
    } 

    return true;
}

function isPostRequest(){
    return $_SERVER["REQUEST_METHOD"] == "POST";
}

function requiredPostValues( array $parameters ){
    foreach($parameters as $parameter){
        if (empty($_POST[$parameter])) {
            return false;
        }
    }
    return true;
}

function getTrackRequestParameters(){

    $start = DateTime::createFromFormat(DATE_FORMAT, $_POST['start']);
    $end   = date(DATE_FORMAT, strtotime($start->format(DATE_FORMAT) . ' +1 day'));

    return [
        '{objectId}'=>$_POST['id'],
        '{begTimestamp}'=>$start->format(DATE_FORMAT),
        '{endTimestamp}'=>$end,
        '{key}'=>$_POST['key']
    ];
}
