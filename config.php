<?php


function platformSlashes($path)
{
    return __DIR__.str_replace('/', DIRECTORY_SEPARATOR, $path);
}

// SYSTEM
define("ROOT_DIRECTORY", __DIR__);
define("DATE_FORMAT", 'Y-m-d');
define("MIN_TRACK_YEAR", 2018);
// TEMPLATES
define("TEMPLATE_MAIN", platformSlashes('/template/base.php'));
define("TEMPLATE_FLEET", platformSlashes('/template/_fleet.php'));
define("TEMPLATE_TRACK", platformSlashes('/template/_track.php'));
define("TEMPLATE_404", platformSlashes('/template/error/404.php'));
define("TEMPLATE_TEST", platformSlashes('/template/test/test.php'));
define("TEMPLATE_TEST_FULL_REQUEST", platformSlashes('/template/test/_full_request.php'));
define("TEMPLATE_TEST_ROUTE", platformSlashes('/template/test/_route.php'));
define("TEMPLATE_ERRORS", platformSlashes('/template/error/_errors.php'));
//CONTROLLERS
define("CONTROLLER_FLEET", platformSlashes('/controller/fleetController.php'));
define("CONTROLLER_TRACK", platformSlashes('/controller/trackController.php'));
define("CONTROLLER_TEST", platformSlashes('/controller/testController.php'));
// MODELS
define("MODEL_FLEET", platformSlashes('/model/Fleet.php'));
define("MODEL_TRACK", platformSlashes('/model/Track.php'));
//SERVICE
define("SERVICE_REQUEST", platformSlashes('/service/requesthandler.php'));
define("SERVICE_FROM_VALIDATION", platformSlashes('/service/formValidation.php'));
//API
define('GOOGLE_CREDENTIALS', 'getyourown');
define('URL_FLEET_REQUEST', 'https://app.ecofleet.com/seeme/Api/Vehicles/getLastData?key={key}&json');
define('URL_TRACK_REQUEST', 'https://app.ecofleet.com/seeme/Api/Vehicles/getRawData?objectId={objectId}&begTimestamp={begTimestamp}&endTimestamp={endTimestamp}&key={key}&json');
