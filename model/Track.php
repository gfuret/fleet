<?php

require 'Request.php';

class Track extends Request
{
    private $route;

    private $hourlyStops;

    private $stops;

    private $distance;


    public function getHourlyStops()
    {
        return $this->hourlyStops;
    }

    public function getStops()
    {
        return $this->stops;
    }

    public function getDistance()
    {
        return $this->distance;
    }

    public function getDailyRoute()
    {
        $this->route = [];
        $this->distance = 0;

        $this->stops = 0;
        $this->lastEngineStatus = 0;

        $distanceStart = 0;
        $distanceStop  = 0;

        foreach ($this->response as $index => $point) {
            if ($index == 0) {
                $distanceStart = isset($point->Distance) ? $point->Distance : 0;
                $this->route[] = ['lat'=>$point->Latitude, 'lng'=>$point->Longitude];
                continue;
            }
            if (empty($distanceStart)) {
                $distanceStart = isset($point->Distance) ? $point->Distance : 0;
            }
            

            if ($this->lastEngineStatus != (int) $this->response[$index]->EngineStatus) {
                if ($this->lastEngineStatus) {
                    $this->stops++;
                    $this->route[] = ['lat'=>$point->Latitude, 'lng'=>$point->Longitude];
                }
                $this->lastEngineStatus = (int) $this->response[$index]->EngineStatus;
            }
        }
        $distanceStop = isset($point->Distance) ? $point->Distance : 0;
        $this->distance = $distanceStop - $distanceStart;

        return $this->route;
    }

    public function getShortestDistance()
    {
    }
}
