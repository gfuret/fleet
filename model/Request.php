<?php
abstract class Request
{
    protected $status;
    protected $response;
    protected $errorMessage;

    function loadFromResponse($request){
        
		$this->setStatus($request->status);
		
        $this->setResponse($request->response);

        if(! empty($request->errormessage)){
            $this->setErrorMessage($request->errormessage);
        }
        
        return $this;
    }

	function getStatus() { 
 		return $this->status; 
	} 

	function setStatus($status) {  
		$this->status = $status; 
	} 

	function getResponse() { 
 		return $this->response; 
	} 

	function setResponse($response) {  
		$this->response = $response; 
	}  

	function getErrorMessage() { 
 		return $this->errorMessage; 
	} 

	function setErrorMessage($errorMessage) {  
		$this->errorMessage = $errorMessage; 
	}  
}