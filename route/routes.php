<?php

switch ($request) {
    case '/' :
    case '' :
        require TEMPLATE_MAIN;
        break;
    case '/fleet' :
        require CONTROLLER_FLEET;
        break;
    case '/track' :
        require CONTROLLER_TRACK;
        break;
    case '/test' :
        require CONTROLLER_TEST;
        break;
    default:
        http_response_code(404);
        require TEMPLATE_404;
        break;
}