<?php

require MODEL_FLEET;
require SERVICE_FROM_VALIDATION;


$errors = validateFleetRequest();

if (! empty($errors)) {
  require TEMPLATE_ERRORS;
  die;
}

$fleetCode = $_POST['fleet'];

try {
  $fleet = getFleetLastLocation( $fleetCode );
} catch (Exception $e) {
  $errors = [$e->getMessage()];
  require TEMPLATE_ERRORS;
  die;
}

$fleet = $fleet->getResponse();

require TEMPLATE_FLEET;