<?php

require MODEL_TRACK;
require SERVICE_FROM_VALIDATION;


$errors = validateTrackRequest();

if (! empty($errors)) {
    errorRequest($errors[0]);
    die;
}

try {
    $track = getDailyTrack(getTrackRequestParameters(), URL_TRACK_REQUEST);
} catch (Exception $e) {
    errorRequest($e->getMessage());
}

if (empty($track->getResponse())) {
    errorRequest('Empty response');
}

echo json_encode([
  'status' => 'success',
  'route' => $track->getDailyRoute() ,
  'stops' => $track->getStops() ,
  'distance' => $track->getDistance() ,
  'shortes-distance' => $track->getShortestDistance() ,
]);
