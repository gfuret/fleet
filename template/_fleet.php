<?php
    if(! empty($errors)){
        foreach($errors as $error) {
?>
    <div class="alert alert-danger" role="alert">
        <?php echo $error; ?>
    </div>
<?php }}else{ ?>
<div class="d-flex justify-content-between">
    <div class="table w-40">
        <table class="w-100">
            <thead>
                <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Speed</th>
                <th scope="col">Last Update</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    foreach($fleet as $index => $vehicle ) { 

                        $selected = $index == 0  ? 'selected-vehicle' : '';
                ?>
                    <tr class="fleet-info <?php echo $selected; ?>" data-lat="<?php echo $vehicle->latitude ; ?>" 
                        data-lng="<?php echo $vehicle->longitude ; ?>" 
                        data-object="<?php echo $vehicle->objectId ; ?>">
                        <th scope="row"><?php echo ($index + 1); ?></th>
                        <td><?php echo $vehicle->objectName; ?></td>
                        <td><?php echo $vehicle->speed; ?></td>
                        <td><?php echo $vehicle->timestamp; ?></td>
                    </tr>
                <?php 
                    } 
                ?>
            </tbody>
        </table>
        
        <div class="mt-3">
            <div><label>Date</label><input type="text" class="form-control" id="travel-date" name="travel-date" ></div>
            <button class="btn btn-primary mt-2" id="get-route"  onclick="getRoute('<?php echo $fleetCode; ?>')" >Show</button>
        </div>


    </div>
    <div class="w-50">
        <div id="map"></div>
    </div>
</div>
    <hr />
    <div class="showme"></div>
    <table class="table w-100" id="daily-result">
        <thead>
            <tr>
                <th>Total Distance</th>
                <th>Number of stops</th>
                <th>Shortest possible distance</th>
            </tr>
        </thead>
        <tbody id="daily-report">
            <tr></tr>
        </tbody>
    </table>

<?php } ?>
<script>
    $(document).ready(function(){

        const today = new Date();
        const thisMonth = today.getMonth() + 1;
        const todayFormatted = ((thisMonth < 10 ? '0' : '') + thisMonth ) +'/'+ ('0' + today.getDate()).slice(-2) + '/' + today.getFullYear();
        
        $('#travel-date').dateDropper({
            format: '<?php echo DATE_FORMAT; ?>',
            minYear: <?php echo MIN_TRACK_YEAR; ?>,
            maxYear: today.getFullYear(),
            defaultDate: todayFormatted,
            large: true
        }).val( '<?php echo date(DATE_FORMAT); ?>' );

        $(".fleet-info").click(function(){
            toggleSelected($(this));
        });

        initMap();
        $('.fleet-info').each(function(){
            addMarker( $(this).data('lat'), $(this).data('lng'), $(this).data('plate'));
        });
    });
</script>