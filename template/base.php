<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Fleet</title>

    <!-- CSS -->

    <link rel='shortcut icon' type='image/x-icon' href='/favicon.ico' />
    <!-- bootstrap lib -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
        integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <!-- input time lib -->
    <!-- <link href="https://res.cloudinary.com/dxfq3iotg/raw/upload/v1587270922/datepicker/datedropper.css" rel="stylesheet"> -->
    <link rel="stylesheet" href="css/custom.css">

    <!-- JS -->
    
    <!-- jquery lib -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <!-- bootstrap lib -->
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
        integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous">
    </script>
    <!-- google map lib -->
    <script src="https://maps.googleapis.com/maps/api/js?key=<?php echo GOOGLE_CREDENTIALS; ?>&libraries=places">
    </script>
    <!-- input time lib -->
    <script src="/js/input-lib/datedropper.pro.min.js"></script>
    <script src="/js/fleet.js"></script>


</head>

<body>
    <div class="container">
        <div class="header-menu">
            <nav class="navbar navbar-light bg-light">
                <form class="form-inline">
                    <input name="fleet-code" id="fleet-code" class="form-control mr-sm-2" type="search"
                        value="home.assignment-699172" aria-label="Search">
                    <button name="fleet-load" id="fleet-load" onclick="getFleet(event);"
                        class="btn btn-outline-success my-2 my-sm-0" type="submit">Check your fleet</button>
                    <div id="spinner" class="spinner-grow ml-3 hide" style="width: 40px; height: 40px;">
                        <span class="sr-only">Loading...</span>
                    </div>
                </form>
            </nav>
        </div>
        <hr />
        <div id="fleet-container" class="m3">
        </div>
    </div>
</body>

</html>
