// LOCATION MAP BEGIN

let map;
let infowindow;

function initMap() {
  const tallinn = new google.maps.LatLng(59.442692, 24.753201);
  infowindow = new google.maps.InfoWindow();
  map = new google.maps.Map(document.getElementById("map"), {
    center: tallinn,
    zoom: 10,
  });
}

function addMarker(lat, lng, vehicle) {
  let marker = new google.maps.Marker({
    position: {
      lat: lat,
      lng: lng,
    },
    map: map,
    title: vehicle,
  });

  let latLng = marker.getPosition();
  map.setCenter(latLng);
}

// LOCATION MAP END

// ROUTE MAP BEGIN



function calcRoute(completeRoute) {

  const directionsService = new google.maps.DirectionsService();
  const directionsRenderer = new google.maps.DirectionsRenderer();

  const map = new google.maps.Map(document.getElementById("map"), {
    zoom: 10,
    center: getStartOfRoute(completeRoute),
  });
  directionsRenderer.setMap(map);

  calculateAndDisplayRoute(
    directionsService,
    directionsRenderer,
    completeRoute
  );
}

function calculateAndDisplayRoute(
  directionsService,
  directionsRenderer,
  completeRoute
) {

  const startPoint = getStartOfRoute(completeRoute);
  const endPoint = getEndOfRoute(completeRoute);

  let waypts = [];

  $.each(completeRoute, function (index, point) {
      waypts.push({
        location: new google.maps.LatLng(point.lat, point.lng),
        stopover: true,
      });
  });

  directionsService.route(
    {
      origin: new google.maps.LatLng(startPoint.lat, startPoint.lng),

      destination: new google.maps.LatLng(endPoint.lat, endPoint.lng),

      waypoints: waypts,

      travelMode: google.maps.TravelMode.DRIVING,
    },
    (response, status) => {
      if (status === "OK") {
        directionsRenderer.setDirections(response);
      } else {
        window.alert("Directions request failed due to " + status);
      }
    }
  );
}

//ROUTE MAP END

// ELEMENT ACTIONS AND EVENTS BEGIN



function getRoute(fleetCode) {
  
  addLoadingAnimation();

  const startDate = $("#travel-date").val();

  const vehicle = $(".selected-vehicle").data("object");

  $.ajax({
    type: "post",
    url: "/track",
    dataType: "JSON",
    data: {
      key: fleetCode,
      start: startDate,
      id: vehicle,
    },
    success: function (response) {
      if (response.status == "success") {
        calcRoute(response.route);
        appendRow(response.distance, response.stops, '?');
      } else {
        alert(response.message);
      }
      stopLoadingAnimation();
    },
  });
}

function appendRow(distance, stops, shortesRoute ){

  $("#daily-report").html("<tr></tr>");
  $("#daily-report tr:last").after(
    "<tr><td>" +
      distance +
      "</td><td>" +
      stops +
      "</td><td>" +
      shortesRoute +
      "</td></tr>"
  );
}

function getStartOfRoute(response) {
  return {
    lat: response[0].lat,
    lng: response[0].lng,
  };
}

function getEndOfRoute(response) {
  return {
    lat: response[response.length - 1].lat,
    lng: response[response.length - 1].lng,
  };
}

function getFleet(event) {
  event.preventDefault();
  let fleetCode = $("#fleet-code").val();
  addLoadingAnimation();

  $.ajax({
    type: "post",
    url: "/fleet",
    data: {
      fleet: fleetCode,
    },
    success: function (data) {
      $("#fleet-container").html(data);
      stopLoadingAnimation();
    },
  });
}

function toggleSelected( row ){
  $(".fleet-info").removeClass("selected-vehicle");
  row.addClass("selected-vehicle");
}

function addLoadingAnimation(){
  $("#spinner").removeClass('hide');
}

function stopLoadingAnimation(){
  $("#spinner").addClass('hide');
}


// ELEMENT ACTIONS AND EVENTS END
